window.addEventListener("load", function() {
	// Circle Graphs
	const mechanicalEfficiencyCircle = new CircleGraph({
		element: document.querySelector(".graph--mechanical-efficiency"),
		percentage: 53
	})

	const cardiorespiratoryFitnessCircle = new CircleGraph({
		element: document.querySelector(".graph--cardiorespiratory-fitness"),
		percentage: 71
	})

	const fastSwitchMusclesCircle = new CircleGraph({
		element: document.querySelector(".graph--fast-switch-muscles"),
		percentage: 45
	})

	const slowSwitchMusclesCircle = new CircleGraph({
		element: document.querySelector(".graph--slow-switch-muscles"),
		percentage: 53
	})

	const fatBurningEfficiencyCircle = new CircleGraph({
		element: document.querySelector(".graph--fat-burning-efficiency"),
		percentage: 79
	})

	const breathingEfficiencyCircle = new CircleGraph({
		element: document.querySelector(".graph--breathing-efficiency"),
		percentage: 85
	})

	// Bar Graphs
	const mechanicalEfficiencyBar = new BarGraph({
		element: document.querySelector(".page--four .page-graph"),
		percentage: 53
	})

	const cardiorespiratoryFitnessBar = new BarGraph({
		element: document.querySelector(".page--six .page-graph"),
		percentage: 71
	})

	const fatBurningEfficiencyBar = new BarGraph({
		element: document.querySelector(".page--eight .page-graph"),
		percentage: 79
	})

	const breathingEfficiencyBar = new BarGraph({
		element: document.querySelector(".page--nine .page-graph"),
		percentage: 85
	})
})

// Functions
function hashGenerator() {
	return Math.random()
		.toString(36)
		.substr(2, 10)
}

// Graph Classes
class BarGraph {
	constructor(options = {}) {
		const { element, percentage } = options
		this.element = element
		this.percentage = percentage || 0
		this.hash = hashGenerator()

		this.makeGraph()
	}

	makeGraph() {
		// Firefox Fix
		let percentageDecimal = this.percentage / 100
		let percentageValue = percentageDecimal * 500

		this.element.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 85" class="bar-graph">
            <defs>
                <mask id="mask_${this.hash}" width="500" height="85" x="0" y="0" maskUnits="userSpaceOnUse">
                    <rect class="meter-bar meter-bar--mask" y="4.83" width="${percentageValue}" height="60" rx="8"/>
                </mask>
            </defs>
                <rect class="background-bar" y="9.83" width="500" height="50" rx="8"/>
                <rect class="meter-bar" y="4.83" width="${percentageValue}" height="60" rx="8"/>
                <line x1="125" y1="-0.17" x2="125" y2="69.83"/>
                <line x1="250" y1="-0.17" x2="250" y2="69.83"/>
                <line x1="375" y1="-0.17" x2="375" y2="69.83"/>
                <text class="bar-text" transform="translate(52.33 40.35)">Low</text>
                <text class="bar-text" transform="translate(166.94 40.35)">Average</text>
                <text class="bar-text" transform="translate(298.62 40.35)">Good</text>
                <text class="bar-text" transform="translate(415.32 40.35)">Excellent</text>
                <text class="legend-text" transform="translate(4.75 82.35)">0</text>
                <text class="legend-text" transform="translate(124.75 82.35)">25</text>
                <text class="legend-text" transform="translate(249.75 82.35)">50</text>
                <text class="legend-text" transform="translate(374.75 82.35)">75</text>
                <text class="legend-text" transform="translate(482.44 82.35)">100</text>
                <g mask="url(#mask_${this.hash})">
                 <text class="bar-text bar-text--hidden" transform="translate(52.33 40.35)">Low</text>
                <text class="bar-text bar-text--hidden" transform="translate(166.94 40.35)">Average</text>
                <text class="bar-text bar-text--hidden" transform="translate(298.62 40.35)">Good</text>
                <text class="bar-text bar-text--hidden" transform="translate(415.32 40.35)">Excellent</text>
                </g>
        </svg>
        `
		this.element.style.setProperty("--perc", this.percentage + "%")
	}
}

class CircleGraph {
	constructor(options = {}) {
		const { element, percentage } = options
		this.element = element
		this.percentage = percentage || 0
		this.percentageValue = this.getPercentageValue()

		this.makeGraph()
	}

	makeGraph() {
		this.element.innerHTML += `
			<svg class="circle-graph" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
				<defs>
					<pattern id="pattern" x="0" y="0" width="500" height="500" patternUnits="userSpaceOnUse">
						<image class="gradient-image" xlink:href="assets/images/circle-gradient.png" x="0" y="0" width="500" height="500"></image>
					</pattern>
				</defs>
				<circle class="background-circle" cx="250" cy="250" r="200"></circle>
				<circle class="foreground-circle" cx="250" cy="250" r="200" style="stroke-dashoffset: ${this.percentageValue};"></circle>
				<circle class="blending-circle" cx="250" cy="250" r="200" style="stroke-dashoffset: ${this.percentageValue};"></circle>
				<text class="graph-value" transform="translate(189.96 276)">${this.percentage}%</text>
			</svg>
		`
	}

	getPercentageValue() {
		return 2 * Math.PI * (200 - this.percentage * 2)
	}
}

class DonutGraph {
	constructor(options = {}) {
		const { element, percentages, brandColor } = options
		this.element = element
		this.percentages = percentages
		this.brandColor = brandColor
		this.segmentOffsets = []
		this.tints = []
		this.hash = hashGenerator()

		this.calculateDashOffsets()
		this.createColorTints()
		this.makeGraph()
	}

	randomHEX() {
		return "#" + ((Math.random() * 0xffffff) << 0).toString(16)
	}

	createColorTints() {
		for (let i = 0; i < this.percentages.length; i++) {
			this.tints.push(this.colorLuminance(this.brandColor, i * 0.1))
		}
	}

	colorLuminance(hex, lum) {
		// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, "")
		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
		}
		lum = lum || 0

		// convert to decimal and change luminosity
		var rgb = "#",
			c,
			i
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2, 2), 16)
			c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16)
			rgb += ("00" + c).substr(c.length)
		}

		return rgb
	}

	calculateDashOffsets() {
		// The default starting point (0) for dashOffset is 3 o clock on a circle
		// That's why we set the initial value to 25 to reset it at 12 o clock
		// dashOffset moves counter-clockwise that's why a positive value moves it back.

		for (let i = 0; i < this.percentages.length; i++) {
			if (i == 0) {
				// The fist slice we want to start from 12 o clock thus we
				// set the 1st offset to 25

				this.segmentOffsets.push(25)
			} else {
				// In order to calculate where each percentage should start we need the following formula
				// Circumference - The previous segment's percentage + The sum of previous segment's
				// Circumference is always 100 (that's explaned by our wierd radius)

				this.segmentOffsets.push(
					100 - this.percentages[i - 1] + this.segmentOffsets[i - 1]
				)
			}
		}
	}

	makeGraph() {
		this.element.innerHTML = `
			<svg width="100%" height="100%" viewBox="0 0 44 44" class="donut-graph">
        <defs>
          <mask id="mask_${this.hash}" width="44" height="44" x="0" y="0" maskUnits="userSpaceOnUse">
            <circle class="donut-mask" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="#fff" stroke-width="10"></circle>
</mask>
        </defs>
				<circle class="donut-background" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="#d2d3d4" stroke-width="10"></circle>
			</svg>
		`

		this.percentages.map((slice, i) => {
			this.element.querySelector("svg").innerHTML += `
				<circle class="donut-segment" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="${
					this.tints[i]
				}" stroke-width="10" stroke-dasharray="${slice} ${100 -
				slice}" stroke-dashoffset="${
				this.segmentOffsets[i]
			}" mask="url('#mask_${this.hash}')"></circle>
			`
		})
	}
}
