export class CircleGraph {
	constructor(options = {}) {
		const { element, percentage } = options
		this.element = element
		this.percentage = percentage || 0
		this.percentageValue = this.getPercentageValue()

		this.makeGraph()
	}

	makeGraph() {
		this.element.innerHTML += `
			<svg class="circle-graph" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
				<defs>
					<pattern id="pattern" x="0" y="0" width="500" height="500" patternUnits="userSpaceOnUse">
						<image class="gradient-image" xlink:href="assets/images/circle-gradient.png" x="0" y="0" width="500" height="500"></image>
					</pattern>
				</defs>
				<circle class="background-circle" cx="250" cy="250" r="200"></circle>
				<circle class="foreground-circle" cx="250" cy="250" r="200" style="stroke-dashoffset: ${this.percentageValue};"></circle>
				<circle class="blending-circle" cx="250" cy="250" r="200" style="stroke-dashoffset: ${this.percentageValue};"></circle>
				<text class="graph-value" transform="translate(189.96 276)">${this.percentage}%</text>
			</svg>
		`
	}

	getPercentageValue() {
		return 2 * Math.PI * (200 - this.percentage * 2)
	}

	// update(newPercentage) {
	// 	this.percentage = newPercentage
	// 	this.percentageValue = this.getPercentageValue()
	// 	this.makeGraph()
	// }
}
