function hashGenerator() {
	return Math.random()
		.toString(36)
		.substr(2, 10)
}

export class DonutGraph {
	constructor(options = {}) {
		const { element, percentages, brandColor } = options
		this.element = element
		this.percentages = percentages
		this.brandColor = brandColor
		this.segmentOffsets = []
		this.tints = []
		this.hash = hashGenerator()

		this.calculateDashOffsets()
		this.createColorTints()
		this.makeGraph()
	}

	randomHEX() {
		return "#" + ((Math.random() * 0xffffff) << 0).toString(16)
	}

	createColorTints() {
		for (let i = 0; i < this.percentages.length; i++) {
			this.tints.push(this.colorLuminance(this.brandColor, i * 0.1))
		}
	}

	colorLuminance(hex, lum) {
		// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, "")
		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
		}
		lum = lum || 0

		// convert to decimal and change luminosity
		var rgb = "#",
			c,
			i
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2, 2), 16)
			c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16)
			rgb += ("00" + c).substr(c.length)
		}

		return rgb
	}

	calculateDashOffsets() {
		// The default starting point (0) for dashOffset is 3 o clock on a circle
		// That's why we set the initial value to 25 to reset it at 12 o clock
		// dashOffset moves counter-clockwise that's why a positive value moves it back.

		for (let i = 0; i < this.percentages.length; i++) {
			if (i == 0) {
				// The fist slice we want to start from 12 o clock thus we
				// set the 1st offset to 25

				this.segmentOffsets.push(25)
			} else {
				// In order to calculate where each percentage should start we need the following formula
				// Circumference - The previous segment's percentage + The sum of previous segment's
				// Circumference is always 100 (that's explaned by our wierd radius)

				this.segmentOffsets.push(
					100 - this.percentages[i - 1] + this.segmentOffsets[i - 1]
				)
			}
		}
	}

	makeGraph() {
		this.element.innerHTML = `
			<svg width="100%" height="100%" viewBox="0 0 44 44" class="donut-graph">
        <defs>
          <mask id="mask_${this.hash}" width="44" height="44" x="0" y="0" maskUnits="userSpaceOnUse">
            <circle class="donut-mask" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="#fff" stroke-width="10"></circle>
</mask>
        </defs>
				<circle class="donut-background" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="#d2d3d4" stroke-width="10"></circle>
			</svg>
		`

		this.percentages.map((slice, i) => {
			this.element.querySelector("svg").innerHTML += `
				<circle class="donut-segment" cx="22" cy="22" r="15.91549430918954" fill="transparent" stroke="${
					this.tints[i]
				}" stroke-width="10" stroke-dasharray="${slice} ${100 -
				slice}" stroke-dashoffset="${
				this.segmentOffsets[i]
			}" mask="url('#mask_${this.hash}')"></circle>
			`
		})
	}
}
