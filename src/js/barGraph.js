function hashGenerator() {
	return Math.random()
		.toString(36)
		.substr(2, 10)
}

export class BarGraph {
	constructor(options = {}) {
		const { element, percentage } = options
		this.element = element
		this.percentage = percentage || 0
		this.hash = hashGenerator()

		this.makeGraph()
	}

	makeGraph() {
		// Firefox Fix
		let percentageDecimal = this.percentage / 100
		let percentageValue = percentageDecimal * 500

		this.element.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 85" class="bar-graph">
            <defs>
                <mask id="mask_${this.hash}" width="500" height="85" x="0" y="0" maskUnits="userSpaceOnUse">
                    <rect class="meter-bar meter-bar--mask" y="4.83" width="${percentageValue}" height="60" rx="8"/>
                </mask>
            </defs>
                <rect class="background-bar" y="9.83" width="500" height="50" rx="8"/>
                <rect class="meter-bar" y="4.83" width="${percentageValue}" height="60" rx="8"/>
                <line x1="125" y1="-0.17" x2="125" y2="69.83"/>
                <line x1="250" y1="-0.17" x2="250" y2="69.83"/>
                <line x1="375" y1="-0.17" x2="375" y2="69.83"/>
                <text class="bar-text" transform="translate(52.33 40.35)">Low</text>
                <text class="bar-text" transform="translate(166.94 40.35)">Average</text>
                <text class="bar-text" transform="translate(298.62 40.35)">Good</text>
                <text class="bar-text" transform="translate(415.32 40.35)">Excellent</text>
                <text class="legend-text" transform="translate(4.75 82.35)">0</text>
                <text class="legend-text" transform="translate(124.75 82.35)">25</text>
                <text class="legend-text" transform="translate(249.75 82.35)">50</text>
                <text class="legend-text" transform="translate(374.75 82.35)">75</text>
                <text class="legend-text" transform="translate(482.44 82.35)">100</text>
                <g mask="url(#mask_${this.hash})">
                 <text class="bar-text bar-text--hidden" transform="translate(52.33 40.35)">Low</text>
                <text class="bar-text bar-text--hidden" transform="translate(166.94 40.35)">Average</text>
                <text class="bar-text bar-text--hidden" transform="translate(298.62 40.35)">Good</text>
                <text class="bar-text bar-text--hidden" transform="translate(415.32 40.35)">Excellent</text>
                </g>
        </svg>
        `
		this.element.style.setProperty("--perc", this.percentage + "%")
	}

	// update(val) {
	// 	this.element.style.setProperty("--perc", this.percentage + "%")
	// }
}
