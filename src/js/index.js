import { CircleGraph } from "./circleGraph"
import { BarGraph } from "./barGraph"

// Circle Graphs
const mechanicalEfficiencyCircle = new CircleGraph({
	element: document.querySelector(".graph--mechanical-efficiency"),
	percentage: 53
})

const cardiorespiratoryFitnessCircle = new CircleGraph({
	element: document.querySelector(".graph--cardiorespiratory-fitness"),
	percentage: 71
})

const fastSwitchMusclesCircle = new CircleGraph({
	element: document.querySelector(".graph--fast-switch-muscles"),
	percentage: 45
})

const slowSwitchMusclesCircle = new CircleGraph({
	element: document.querySelector(".graph--slow-switch-muscles"),
	percentage: 53
})

const fatBurningEfficiencyCircle = new CircleGraph({
	element: document.querySelector(".graph--fat-burning-efficiency"),
	percentage: 79
})

const breathingEfficiencyCircle = new CircleGraph({
	element: document.querySelector(".graph--breathing-efficiency"),
	percentage: 85
})

// Bar Graphs
const mechanicalEfficiencyBar = new BarGraph({
	element: document.querySelector(".page--four .page-graph"),
	percentage: 53
})

const cardiorespiratoryFitnessBar = new BarGraph({
	element: document.querySelector(".page--six .page-graph"),
	percentage: 71
})

const fatBurningEfficiencyBar = new BarGraph({
	element: document.querySelector(".page--eight .page-graph"),
	percentage: 79
})

const breathingEfficiencyBar = new BarGraph({
	element: document.querySelector(".page--nine .page-graph"),
	percentage: 85
})
