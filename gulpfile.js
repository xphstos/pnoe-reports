const gulp = require("gulp")
const plumber = require("gulp-plumber")
const sass = require("gulp-sass")
const postcss = require("gulp-postcss")
const postcssPresetEnv = require("postcss-preset-env")
const autoprefixer = require("autoprefixer")
const browserSync = require("browser-sync").create()
const tap = require("gulp-tap")
const buffer = require("vinyl-buffer")
const browserify = require("browserify")
const babelify = require("babelify")
const source = require("vinyl-source-stream")
const uglify = require("gulp-uglify-es")
const pug = require("gulp-pug")

const paths = {
	styles: {
		src: "./src/scss/style.scss",
		watch: "./src/scss/**/*.scss",
		dest: "./dist/"
	},
	js: {
		src: "./src/js/**/*.js",
		watch: "./src/**/*.js",
		dest: "./dist/js/"
	},
	html: {
		src: "./src/**/*.html",
		dest: "./dist/"
	},
	assets: {
		src: "./src/assets/**/*",
		dest: "./dist/assets/"
	},
	pug: {
		src: "./src/index.pug",
		watch: "./src/**/*.pug",
		dest: "./dist/"
	}
}

function serve() {
	browserSync.init({
		server: {
			baseDir: "./dist/"
		},
		notify: false,
		open: false
		// browser: "google chrome canary"
	})
	gulp.watch(paths.styles.watch, styles)
	gulp.watch(paths.assets.src, assets).on("change", browserSync.reload)
	gulp.watch(paths.html.src, html).on("change", browserSync.reload)
	gulp.watch(paths.pug.watch, pugpug).on("change", browserSync.reload)
	gulp.watch(paths.js.src, scripts).on("change", browserSync.reload)
}

exports.serve = serve

function html() {
	return gulp
		.src(paths.html.src)
		.pipe(plumber())
		.pipe(gulp.dest(paths.html.dest))
}
exports.html = html

function pugpug() {
	return gulp
		.src(paths.pug.src)
		.pipe(plumber())
		.pipe(pug({ pretty: true }))
		.pipe(gulp.dest(paths.pug.dest))
}

exports.pugpug = pugpug

function assets() {
	return gulp
		.src(paths.assets.src)
		.pipe(plumber())
		.pipe(gulp.dest(paths.assets.dest))
}

exports.assets = assets

function styles() {
	return gulp
		.src(paths.styles.src)
		.pipe(plumber())
		.pipe(sass({ outputStyle: "expanded" }))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(
			postcss([
				autoprefixer({ grid: false }),
				postcssPresetEnv({
					stage: 2
				})
			])
		)
		.pipe(browserSync.stream())
}
exports.style = styles

function scripts() {
	return gulp
		.src(paths.js.src, { read: false })
		.pipe(
			tap(function(entry) {
				entry.contents = browserify(entry.path, {
					debug: false,
					sourceMaps: false
				})
					.transform(babelify, {
						presets: ["@babel/preset-env"],
						sourceMaps: false
					})
					// debug: false,
					.bundle()
			})
		)
		.pipe(buffer())
		.pipe(gulp.dest(paths.js.dest))
		.pipe(browserSync.stream())
}

exports.scripts = scripts

exports.default = gulp.series(pugpug, assets, styles, scripts, serve)
